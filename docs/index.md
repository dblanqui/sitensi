# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

!!! note "Définition"
    ``` python
    def bubble_sort(items):
        for i in range(len(items)):
            for j in range(len(items) - 1 - i):
                if items[j] > items[j + 1]:
                    items[j], items[j + 1] = items[j + 1], items[j]
    ```
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

![Placeholder](https://dummyimage.com/400x300/eee/aaa){ align=left }
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus aucto massa, nec semper lorem quam in massa.\        
The homomorphism $f$ is injective if and only if its kernel is only the singleton set $e_G$, because otherwise $\exists a,b\in G$ with $a\neq b$ such that $f(a)=f(b)$.  
<div style="clear: both;"></div>  

## Test  

=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```
<iframe   width="100%"
height="642" name="basthon" src="https://console.basthon.fr/?script=eJyNVNtu2zAMfR-wf2AzDLE3x7WbFQWC2C-7AH0btr4bsk2nAmTJ1SXYPinfkR8bZTuXNSkSA4kt6RzykBRZYwM1X3PDlSzQVYLXHKXEgEVluHj_DuiZTCYBlzaiXwizHKzrBO53BsxPjZWSNbdkxyyAQZ5BAmihhJw-2Bor2uzXKC1HbeKB-JuDQAOd3m4OBkAiGCUtdMyARtNhZbcbNBHUilvakWv1FzXM0hh-KFl5EmDTEMwxogm2DwqOgoIaSUTHdK8CnOWCG8IPSsSUiZXS3D632IOMcsZq1luP4Zd3yhGcJBGuFBRL8OIUxUKJII0WQ5KHgy2jtEV4cd5fBjvYpxI-Qw8dEpLAMhvXSyjHhHylkCdS6Za5PxOv-FwkAzTP8_PFS5MI7sbKBPcRJOEFwl0E6Q4UHJHfJMwfIpjvCKmnj4v_qsr0yrUUuBmqKbebFbO8Mf4Wfdcanb7gZuYjeRhNz9Irwp69Rr8h6PiGjTfyalVpEs8j-HK1rJRyFd-f1fVtV1wqdHLBTnKU8uRQ1FeG_M29ZGl-YF9Un5xiaSAMH7wBlmXU3rKG8iZLxoHhH43WaUnsHRmFRy8TUNR-48tz6S2VBXIsjWWy8qOnnzSnB-VwcOJlLwyFwTPHAbu9LSP2sQzhAzwKWDNomBCKa6Cpo7nuW1X4dq2elaHuIqlFIVmLRQFZBtOiaBmXRTFdjOnmbUctTpOgstS_w964iP1fq-pgjboka9mTdhj-A6F4lRs"></iframe>